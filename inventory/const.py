from enum import IntEnum

class UserType(IntEnum):
    MANAGER = 1
    ASSISTANCE = 2


class PermissionType(IntEnum):
    GET = 1
    POST = 2
    PUT = 3
    DELETE = 4