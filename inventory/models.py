from django.db import models

# Create your models here.


class Inventory(models.Model):
    product_id = models.AutoField(primary_key=True)
    product_name = models.CharField(max_length=100, null=True, blank=True)
    vendor = models.CharField(max_length=100, null=True, blank=True)
    mrp = models.IntegerField(default=-1, null=True, blank=True)
    batch_num = models.CharField(max_length=100,default='', null=True, blank=True)
    batch_date = models.DateTimeField(auto_now_add=True, null=True,blank=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    status = models.IntegerField(default=0, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True,blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, null=True,blank=True)

    def __unicode__(self):
        return str(self.date_created) + ' : '+str(self.date_updated) + ' : '+str(self.product_id) + ' : '+str(self.product_name) + ' : '+ str(self.vendor)+ ' : ' +str(self.batch_date) + ' : ' + str(self.quantity) + ' : ' + str(self.status) + ' : ' + str(self.batch_num)

    #these variables will be consumed by front end for delete/update operations
    @property
    def delete_option_val(self):
        return self.product_id

    @property
    def update_option_val(self):
        return self.product_id

