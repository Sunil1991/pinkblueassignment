from django.conf.urls import url, include
from rest_framework import routers

from .views import InventoryViewset, HandlePermissionViewSet

router = routers.DefaultRouter()
router.register(r'inventory', InventoryViewset)
router.register(r'permission', HandlePermissionViewSet)

slashless_router = routers.DefaultRouter(trailing_slash=False)
slashless_router.registry = router.registry[:]

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(slashless_router.urls)),
]