from rest_framework import serializers

from usermgmt.models import PermissionRequest
from .models import Inventory


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = ('product_id','product_name','vendor','mrp','batch_num','batch_date','quantity','status','date_created','date_updated','delete_option_val','update_option_val')


class PermissionRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = PermissionRequest
        fields = ('requested_by','approved_by','permission_type','date_created','date_updated')