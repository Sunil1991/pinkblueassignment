from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response

from usermgmt.models import Permission, PermissionRequest
from .models import Inventory
from rest_framework import viewsets, status
from .const import UserType
from .serializers import InventorySerializer, PermissionRequestSerializer
from django.contrib.auth.decorators import login_required

@login_required
def Home(request):
    template = 'inventory.html'
    permissionObj = Permission.objects.get(user_id=request.user.id)
    context = {'inventory_count': Inventory.objects.count(),'read':permissionObj.get,'write':permissionObj.post,'delete':permissionObj.delete,'update':permissionObj.put}
    return render(request, template, context)

class HandlePermissionViewSet(viewsets.ModelViewSet):
    # authentication_classes = (SessionCsrfExemptAuthentication, BasicAuthentication, TokenAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = PermissionRequestSerializer
    allowed_methods = ('GET', 'POST', 'PUT',)
    queryset = PermissionRequest.objects.all()

    def get_queryset(self):
        queryset = PermissionRequest.objects.all()
        return queryset

    def list(self, request, *args, **kwargs):
        permissionRequestQueryset = PermissionRequest.objects.all()
        serializer = PermissionRequestSerializer(
            permissionRequestQueryset, many=True, context={'request': request})
        result = dict()
        result ['data'] = serializer.data
        return Response(result, content_type="application/json")

    def create(self, request, *args, **kwargs):
        if request.user.user_type == UserType.ASSISTANCE.value:
            response = super(HandlePermissionViewSet, self).create(
                request, *args, **kwargs)
            return response
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class InventoryViewset(viewsets.ModelViewSet):
    # authentication_classes = (SessionCsrfExemptAuthentication, BasicAuthentication, TokenAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = InventorySerializer
    allowed_methods = ('GET', 'POST', 'PUT',)
    queryset = Inventory.objects.all()

    def get_queryset(self):
        queryset = Inventory.objects.all()
        return queryset

    def list(self, request, *args, **kwargs):
        inventoryQueryset = Inventory.objects.all()
        serializer = InventorySerializer(
            inventoryQueryset, many=True, context={'request': request})
        result = dict()
        result ['data'] = serializer.data
        return Response(result, content_type="application/json")

    def create(self, request, *args, **kwargs):
        if request.user.is_staff:
            response = super(InventoryViewset, self).create(
                request, *args, **kwargs)
            return response
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def update(self, request, *args, **kwargs):
        product_id = int(kwargs['pk'])
        try:
            inventoryObject = Inventory.objects.get(product_id=product_id)
        except:
            inventoryObject = None
        if inventoryObject:
            existingObject = inventoryObject
            for key, value in request.data.iteritems():
                if value is None:
                    if getattr(existingObject, key) is not None:
                        value = getattr(existingObject, key)
                setattr(inventoryObject, key, value)
                inventoryObject.save()
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        pk = int(kwargs['pk'])
        try:
            Inventory.objects.get(id=pk).delete()
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
                return Response({'result': 'Failed to find object',
                                 'user_msg': 'Unable to retrieve record.'},
                                status=status.HTTP_400_BAD_REQUEST)

