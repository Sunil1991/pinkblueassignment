

# Create your models here.


from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin, UserManager)
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class User(AbstractBaseUser,PermissionsMixin):
    username = models.CharField(_('username'), max_length=50, unique=True, null=True)
    email = models.EmailField(_('email address'), max_length=100, unique=True)
    first_name = models.CharField(_('first name'), max_length=100, blank=True)
    last_name = models.CharField(_('last name'), max_length=100, blank=True)
    designation = models.CharField(_('designation'), max_length=100, blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now, null=True, blank=True)
    date_updated = models.DateTimeField(auto_now=True, null=True, blank=True)
    user_type = models.IntegerField(default = -1, null=True, blank=True)
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_short_name(self):
        if self.first_name:
            return self.first_name.strip()
        else:
            return 'Unknown'

    def __unicode__(self):
        if self.first_name:
            return self.first_name.strip()
        elif self.username:
            return self.username.strip()
        elif self.email:
            return self.email.strip()
        else:
            return 'unknown'

    def get_absolute_url(self):
        return "/users/%s/" % self.username

    def get_first_name(self):
        first_name = self.first_name
        return first_name.strip()

    def get_last_name(self):
        return self.last_name.strip()

class Permission(models.Model):
    user_id = models.IntegerField(default = -1, null=True, blank=True)
    get = models.BooleanField( default=True,blank=True)
    put = models.BooleanField(default=False,blank=True)
    post = models.BooleanField(default=False,blank=True)
    delete = models.BooleanField(default=False,blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __unicode__(self):
        return str(
            self.get) + ' : ' + str(self.put) + ' : ' + str(self.post) + ' : ' + str(
            self.delete) + ' : ' + str(self.date_created)


class PermissionRequest(models.Model):
    requested_by = models.IntegerField(default = -1, null=True, blank=True)
    approved_by = models.IntegerField(default=-1, null=True, blank=True)
    permission_type = models.IntegerField(default = -1, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    date_updated = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __unicode__(self):
        return str(self.requested_by) + ' : ' + str(self.approved_by) + ' : ' + str(
            self.permission_type)  + str(self.date_created)
