CREATE SEQUENCE inventory_inventory_pkey
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;


CREATE TABLE public.inventory_inventory
(
  product_id integer NOT NULL DEFAULT nextval('inventory_inventory_id_seq'::regclass),
  product_name character varying(100),
  vendor character varying(100),
  mrp integer,
  batch_num character varying(100),
  batch_date timestamp with time zone,
  quantity integer,
  date_created timestamp with time zone,
  date_updated timestamp with time zone,
  status integer,
  CONSTRAINT inventory_inventory_pkey PRIMARY KEY (product_id)
)

CREATE SEQUENCE usermgmt_permission_pkey
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.usermgmt_permission
(
  id integer NOT NULL DEFAULT nextval('usermgmt_permission_id_seq'::regclass),
  user_id integer,
  requested_by integer,
  approved_by integer,
  get boolean,
  put boolean,
  post boolean,
  delete boolean,
  date_created timestamp with time zone,
  date_updated timestamp with time zone,
  CONSTRAINT usermgmt_permission_pkey PRIMARY KEY (id)
)

CREATE SEQUENCE usermgmt_permissionrequest_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.usermgmt_permissionrequest
(
  requested_by integer DEFAULT '-1'::integer,
  permission_type integer DEFAULT '-1'::integer,
  approved_by integer DEFAULT '-1'::integer,
  date_created date,
  date_updated date,
  id integer NOT NULL DEFAULT nextval('usermgmt_permissionrequest_id_seq'::regclass)
)



INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('Dalton Sofa Set 3 Seater','Amazon',25000,'TYU5421','2019-12-29',17,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('ALDA OTTOMAN-FB-MLTI COLOUR','Flipkart',5000,'NHY4319','2019-12-24',10,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('TERENCE KING BED WITH HYDRAULIC STORAG','Snapdeal',10000,'BNM1209','2019-12-24',20,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('MOD.KITCHEN. COMPLETE SET','Amazon',50000,'ZAW6753','2019-12-27',10,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('Hanging Lamps IH0B808','Flipkart',4000,'VCX7109','2019-12-27',15,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('ANTALITA 3 SEATER SOFA','Flipkart',20000,'MLU6510','2019-12-27',20,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('Jali Dining Set 4 Seater Set (Table + 4 Chairs)','Flipkart',26000,'BGT2581','2019-12-27',5,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('COLTON SOFA 3 SEATER FB BROWN','Flipkart',5000,'VFR5678','2019-12-27',12,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('KNOX CABINET','Flipkart',30000,'MKI4517','2019-12-27',20,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('Reasure Box Treasure Box','Amazon',10000,'VRW1415','2019-12-26',10,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');


INSERT INTO public.inventory_inventory (product_name, vendor, mrp, batch_num, batch_date, quantity, date_created, date_updated)
VALUES ('Lamp Table dining set','Flipkart',20000,'CFT1234','2019-12-26',12,'2019-12-28 14:26:22.508268+05:30','2019-12-28 14:26:22.508268+05:30');